#ifndef __UART_DEV_H__
#define __UART_DEV_H__


#ifdef __cplusplus
#if __cplusplus
extern "C" {
#endif /* __cplusplus */
#endif /* __cplusplus */

#ifndef FALSE
#define FALSE    (0)
#endif

#ifndef TRUE
#define TRUE    (1)
#endif




#define CONFIG_MAX_BAUDRATE    921600 /* max baud rate */


/* trace */
#define  TRACE_INIT    (1<<0)
#define  TRACE_DATA    (1<<2)
#define  TRACE_MSG     (1<<3)
#define  TRACE_ERR     (1<<4)

#define TRACE_MASK     (0xff)
/* messages print definitions, for debug ,err e.g. */
#ifdef LOSCFG_DEBUG_VERSION
#define uart_trace(mask, msg...) do { \
        if ((mask) & TRACE_MASK) { \
            dprintf("<uart>,%s:%d: ", __func__, __LINE__); \
            dprintf(msg); \
            dprintf("\n"); \
        } \
    } while (0)
#else
#define uart_trace(mask, msg...)
#endif

#define uart_error(msg...) do { \
    dprintf("<uart,err>:%s,%d: ", __func__, __LINE__); \
    dprintf(msg); \
    dprintf("\n"); \
}while(0)



/*
 * uart core functions
 * */
/* read some data from rx_data buf in uart_ioc_transfer */
int uart_dev_read(struct uart_driver_data *udd, char *buf, size_t count);
/* check the buf is empty */
int uart_rx_buf_empty(struct uart_driver_data *udd);

int uart_recv_notify(struct uart_driver_data *udd, const char *buf, size_t count);


#ifdef __cplusplus
#if __cplusplus
}
#endif /* __cplusplus */
#endif /* __cplusplus */

#endif /* __UART_DEV_H__ */
