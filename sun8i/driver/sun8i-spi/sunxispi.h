#ifndef __sunxispi_h__
#define __sunxispi_h__

#include <spi.h>

void sunxi_spi_init(void);
struct SpiBus *sunxi_spi_bus_get(int bus);
struct SpiDev *sunxi_spi_dev_get(int bus, int dev);

#endif