SUN8I_BASE_DIR 	:= $(LITEOSTOPDIR)/../../vendor/allwinner/sun8i

LIB_SUBDIRS     += $(SUN8I_BASE_DIR)/board
LITEOS_BASELIB 	+= -lboard

LIB_SUBDIRS		+= $(SUN8I_BASE_DIR)/driver/sun8i-eth
LITEOS_BASELIB	+= -lsun8i-eth

LIB_SUBDIRS		+= $(SUN8I_BASE_DIR)/driver/sun8i-fb
LITEOS_BASELIB	+= -lsun8i-fb

LIB_SUBDIRS		+= $(SUN8I_BASE_DIR)/driver/dw-uart
LITEOS_BASELIB	+= -ldw-uart

LIB_SUBDIRS		+= $(SUN8I_BASE_DIR)/driver/sun8i-spi
LITEOS_BASELIB	+= -lsun8i-spi
LITEOS_DRIVERS_INCLUDE += -I$(SUN8I_BASE_DIR)/driver/sun8i-spi

LITEOS_MTD_SPI_NOR_INCLUDE += 	-I$(SUN8I_BASE_DIR)/driver/mtd/common/include \
								-I$(SUN8I_BASE_DIR)/driver/mtd/spi_nor/include
