#ifndef __SUN8I_UART_H__
#define __SUN8I_UART_H__

#include "asm/platform.h"
#include "los_typedef.h"
#include "los_base.h"
#ifdef __cplusplus
#if __cplusplus
extern "C" {
#endif /* __cplusplus */
#endif /* __cplusplus */

extern void uart_init(void);
extern void uart_interrupt_unmask(void);
extern int uart_hwiCreate(void);
extern UINT8 uart_getc(void);
extern char uart_fputc(char c, void *f);

extern UINT32 UartPutsReg(UINTPTR base, const CHAR *s, UINT32 len, BOOL isLock);
extern VOID UartPuts(const CHAR *s, UINT32 len, BOOL isLock);

#ifdef __cplusplus
#if __cplusplus
}
#endif /* __cplusplus */
#endif /* __cplusplus */

#endif
