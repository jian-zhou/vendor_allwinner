
#ifndef __BOARD_CONFIG_H__
#define __BOARD_CONFIG_H__

#include "menuconfig.h"

#ifdef __cplusplus
#if __cplusplus
extern "C" {
#endif /* __cplusplus */
#endif /* __cplusplus */

/* physical memory base and size */
#define DISPLAY_XRES            800
#define DISPLAY_YRES            480
#define DISPLAY_GRAM_ADDR       0x43e89000
#define DISPLAY_GRAM_SIZE       (((DISPLAY_XRES*DISPLAY_YRES*4) + 0xfff) & (~0xfff))

#define DDR_MEM_ADDR            0x40000000
// #define DDR_MEM_SIZE            0x04000000
#define DDR_MEM_SIZE            0x03e00000

/* Peripheral register address base and size */
#define PERIPH_PMM_BASE         0x01000000
#define PERIPH_PMM_SIZE         0x01000000

#define KERNEL_VADDR_BASE       0x40000000
#define KERNEL_VADDR_SIZE       DDR_MEM_SIZE

#define SYS_MEM_BASE            DDR_MEM_ADDR
// #define SYS_MEM_SIZE_DEFAULT    0x4000000
#define SYS_MEM_SIZE_DEFAULT    0x03e00000
#define SYS_MEM_END             (SYS_MEM_BASE + SYS_MEM_SIZE_DEFAULT)

#define EXC_INTERACT_MEM_SIZE        0x100000

#ifdef __cplusplus
#if __cplusplus
}
#endif /* __cplusplus */
#endif /* __cplusplus */

#endif
