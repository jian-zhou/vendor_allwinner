#ifndef __DMA_H
#define __DMA_H

#ifdef __cplusplus
#if __cplusplus
extern "C" {
#endif /* __cplusplus */
#endif /* __cplusplus */

extern void dma_cache_clean(UINTPTR start, UINTPTR end);
extern void dma_cache_inv(UINTPTR start, UINTPTR end);

#ifdef __cplusplus
#if __cplusplus
}
#endif /* __cplusplus */
#endif /* __cplusplus */


#endif /* __DMA_H */

