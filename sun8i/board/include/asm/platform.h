#ifndef    __ASM_PLATFORM_H__
#define    __ASM_PLATFORM_H__

#include "menuconfig.h"
#include "los_bitmap.h"
#include "asm/hal_platform_ints.h"
#include "soc/uart.h"

#ifdef __cplusplus
#if __cplusplus
extern "C" {
#endif /* __cplusplus */
#endif /* __cplusplus */

/*------------------------------------------------
 *GIC reg base address
 *------------------------------------------------*/
#define GIC_BASE_ADDR				IO_DEVICE_ADDR(0x01C80000)

#define GICD_OFFSET               0x1000                          /* interrupt distributor offset */
#define GICC_OFFSET               0x2000                          /* CPU interface register offset */

#define DDR_MEM_BASE              0x40000000


#define UART_REG_ADDR             0x01C28000
#define UART0_REG_PBASE           (UART_REG_ADDR + 0x000)
#define UART0_REG_BASE            IO_DEVICE_ADDR(UART0_REG_PBASE)

#define FMC_REG_PBASE             0x01c68000
#define FMC_REG_OFFSIZE           0x1000

#define NUM_HAL_INTERRUPT_HRTIMER 83

#define CACHE_ALIGNED_SIZE        64

#ifdef __cplusplus
#if __cplusplus
}
#endif /* __cplusplus */
#endif /* __cplusplus */

#endif
