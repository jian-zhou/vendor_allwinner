#ifndef _RESET_SHELL_H
#define _RESET_SHELL_H
#include "los_task.h"

#ifdef __cplusplus
#if __cplusplus
extern "C" {
#endif /* __cplusplus */
#endif /* __cplusplus */


typedef VOID* (*STORAGE_HOOK_FUNC)(VOID*);

typedef struct tagHookFuncNode {
    STORAGE_HOOK_FUNC  pHandler;
    VOID *pParam;
    struct tagHookFuncNode *pNext;
}Hook_Func_Node;

extern Hook_Func_Node *g_hook_func_node;

UINT32 osReHookFuncAdd(STORAGE_HOOK_FUNC handler, VOID *param);
UINT32 osReHookFuncDel(STORAGE_HOOK_FUNC handler);
VOID osReHookFuncHandle(VOID);
extern void cmd_reset(void);

#ifdef __cplusplus
#if __cplusplus
}
#endif /* __cplusplus */
#endif /* __cplusplus */

#endif
